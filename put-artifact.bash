#!/usr/bin/env bash

THIS_SCRIPT="$(readlink -f "$0")"
THIS_DIR="$(dirname "${THIS_SCRIPT}")"

echo -e "This is a custom artifact.\nIt is being published as an OCI artifact." > ./demo.txt
regctl registry login $CI_REGISTRY --user $CI_REGISTRY_USER --pass $CI_REGISTRY_PASSWORD
regctl registry set $CI_REGISTRY --tls insecure
echo -n "{}" | regctl blob put $CI_REGISTRY_IMAGE/artifact-type:demo
regctl blob put $CI_REGISTRY_IMAGE/artifact-type:demo < ./demo.txt

regctl manifest put $CI_REGISTRY_IMAGE/artifact-type:demo < $1
