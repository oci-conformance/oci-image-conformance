FROM debian:latest

RUN apt update && \
    apt install -yy \
      curl && \
    rm -rf /var/lib/apt/lists/*

RUN curl -L https://github.com/regclient/regclient/releases/latest/download/regctl-linux-amd64 > /usr/bin/regctl && \
  chmod 755 /usr/bin/regctl

