#!/usr/bin/env bash

readonly SCRIPT_FILENAME=$(readlink -f "${BASH_SOURCE[0]}")
readonly SCRIPT_PATH=$(dirname "$SCRIPT_FILENAME")

# output formatting
readonly OUTPUT_RESET="\e[0m"   # regular output
readonly OUTPUT_BOLD="\e[1m"    # bold/bright
readonly OUTPUT_UNDER="\e[4m"   # underlined
readonly OUTPUT_RED="\e[31m"    # red
readonly OUTPUT_GREEN="\e[32m"  # green
readonly OUTPUT_YELLOW="\e[33m" # yellow
readonly OUTPUT_BLUE="\e[34m"   # blue

set -o nounset   # do not allow unset variables (-u)
set -o pipefail  # fail if any command in a pipeline fails
#set -o errexit  # exit script if a command fails (-e)
#set -o xtrace   # print each command for debugging (-x)

source test-cr.conf

echof(){
    printf "$1%s$OUTPUT_RESET\n" "$2"
}

warn(){
    >&2 echof "$OUTPUT_YELLOW" "$1"
}

error(){
    >&2 echof "$OUTPUT_RED" "$1"
}

fatal(){
    error "$1"
    exit 1
}

put_artifact(){
    local JOB_NAME=$1
    local SPEC_INFO=$2

    # echo "${SPEC_INFO}"
    # cat $JOB_NAME.json

    ./put-artifact.bash $JOB_NAME.json &> /dev/null
    if [[ $? -eq 0 ]]; then
        echo "  $JOB_NAME: succeeded."
    else
        warn "  $JOB_NAME: failed!"
    fi
}

put_artifact_manifest(){
    put_artifact $@
}

put_artifact_config(){
    put_artifact $@
}

put_artifact_layers(){
    put_artifact $@
}

no_manifest_media_type(){
    local JOB_NAME="no-manifest-media-type"
    local SPEC_INFO='OCI Image Specification - Manifest -> https://github.com/opencontainers/image-spec/blob/v1.1.0/manifest.md
Specification says:
    mediaType [...] This property SHOULD be used [...]
Therefore not specifying this property MUST be supported.
    '

    put_artifact_manifest $JOB_NAME $SPEC_INFO
}

default_media_type(){
    local JOB_NAME="default-media-type"
    local SPEC_INFO='OCI Image Specification - Manifest -> https://github.com/opencontainers/image-spec/blob/v1.1.0/manifest.md
Specification says:
    mediaType [...] when used, this field MUST contain [...] application/vnd.oci.image.manifest.v1+json [...]
    '

    put_artifact_manifest $JOB_NAME $SPEC_INFO
}

default_config_type(){
    local JOB_NAME="default-config-type"
    local SPEC_INFO='OCI Image Specification - Manifest -> https://github.com/opencontainers/image-spec/blob/v1.1.0/manifest.md
Specification says:
    config/mediaType [...] Implementations MUST support at least the following media types: application/vnd.oci.image.config.v1+json [...]
    '

    put_artifact_config $JOB_NAME $SPEC_INFO
}

empty_config_file_and_artifact_type(){
    local JOB_NAME="empty-config-file-and-artifact-type"
    local SPEC_INFO='OCI Image Specification - Manifest -> https://github.com/opencontainers/image-spec/blob/v1.1.0/manifest.md
Specification says:
    artifactType [...] This MUST be set when config.mediaType is set to the empty value [...]
    '

    put_artifact_manifest $JOB_NAME $SPEC_INFO
}

artifact_type_over_config_type(){
    local JOB_NAME="artifact-type-over-config-type"
    local SPEC_INFO='OCI Image Specification - Manifest -> https://github.com/opencontainers/image-spec/blob/v1.1.0/manifest.md
Specification says:
    config/mediaType [...] MUST NOT error on encountering a value that is unknown to the implementation [...]
    '

    put_artifact_config $JOB_NAME $SPEC_INFO
}

blob_media_type(){
    local JOB_NAME="blob-media-type"
    local SPEC_INFO='OCI Image Specification - Manifest -> https://github.com/opencontainers/image-spec/blob/v1.1.0/manifest.md
Specification says:
    layers/mediaType [...] MUST NOT error on encountering a mediaType that is unknown to the implementation [...]
    '

    put_artifact_layers $JOB_NAME $SPEC_INFO
}

put_artifact_fails(){
    local JOB_NAME=$1
    local SPEC_INFO=$2

    # echo "${SPEC_INFO}"
    # cat $JOB_NAME.json

    ./put-artifact.bash $JOB_NAME.json &> /dev/null
    if [[ $? -ne 0 ]]; then
        echo "  $JOB_NAME: failed as expected."
    else
        warn "  $JOB_NAME: did not fail as expected!"
    fi
}

put_artifact_fails_manifest(){
    put_artifact_fails $@
}

put_artifact_fails_config(){
    put_artifact_fails $@
}

wrong_manifest_media_type_fails(){
    local JOB_NAME="wrong-manifest-media-type-fails"
    local SPEC_INFO='OCI Image Specification - Manifest -> https://github.com/opencontainers/image-spec/blob/v1.1.0/manifest.md
Specification says:
    mediaType [...] when used, this field MUST contain [...] application/vnd.oci.image.manifest.v1+json [...]
    '

    put_artifact_fails_manifest $JOB_NAME $SPEC_INFO
}

empty_config_file_fails(){
    local JOB_NAME="empty-config-file-fails"
    local SPEC_INFO='OCI Image Specification - Manifest -> https://github.com/opencontainers/image-spec/blob/v1.1.0/manifest.md
Specification says:
    artifactType [...] This MUST be set when config.mediaType is set to the empty value [...]
    '

    put_artifact_fails_manifest $JOB_NAME $SPEC_INFO
}


echo "Manifest:"

no_manifest_media_type
default_media_type
empty_config_file_and_artifact_type

wrong_manifest_media_type_fails
empty_config_file_fails

echo "Config:"

default_config_type
artifact_type_over_config_type

echo "Layers:"

blob_media_type
